<?php

namespace App\Controller;

use App\Entity\User;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class DefaultController extends Controller
{

    /**
     * TODO: Add the double asterisk in the line above for this route to work
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {
        //return new Response("hello world");

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add('name', TextType::class)
            ->add('surname', TextType::class)
            ->add('birthdate', DateType::class, array(
                'format' => 'yyyy-MM-dd',
                'years' => range(1900, 2018)
            ))
            ->add('gender', ChoiceType::class, array(
                'choices' => array(
                    'Male' => 'M',
                    'Female' => 'F'
                ),
                'expanded' => true,
                'multiple' => false
            ))
            ->add('description', TextareaType::class)
            ->add('save', SubmitType::class, array('label' => 'Save'))
            ->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $this->container->get('session')->set('user', $user);

            return $this->redirectToRoute('view');
        }

        return $this->render('user.html.twig', array('form' => $form->createView()));
    }

    /**
     * @Route("/view", name="view")
     */
    public function view(Request $request) {
        $user = $this->container->get('session')->get('user');
        if ($user === null || !$user instanceof User) {
            return $this->createNotFoundException();
        }

        return $this->render('view.html.twig', array('user' => $user));
        //return new Response("username " . $user->getName());
    }

}
        /*
        $time_start = microtime(true);
        for ($i=0; $i<1000; $i++) {
            $file = fopen(__DIR__, "r");
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo '<b>File system IO speed:</b> '.$execution_time.' Secs ' . '<br>';

        $time_start = microtime(true);
        for ($i=0; $i<3; $i++) {
            $file = file_get_contents("http://www.google.com");
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo '<b>Network speed:</b> '.$execution_time.' Secs ' . '<br>';

        $time_start = microtime(true);
        for ($i=0; $i<10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {g
                foreach ($b as $j) {
                    $c[] = $h*$j;
                }
            }
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        echo '<b>PHP execution speed:</b> '.$execution_time.' Secs ' . '<br>';
        */